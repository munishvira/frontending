function getRetailers() 	
	{
	ITZCPMS.getRetailerInfomation(createRetailers);
	}
	function createRetailers(RetailersName) 
	{
		
		var retailerId = DWRUtil.getValue("retailerId");
		DWRUtil.removeAllOptions("retailerId");
  		DWRUtil.addOptions("retailerId",RetailersName);
  		DWRUtil.setValue("retailerId",retailerId);
  		
  		
	}
	function getrmtBank() 	
	{
		ITZCPMS.getRmtBankList(creatermtBank);
	}
	function creatermtBank(bankName) 
	{   
		var bankId = DWRUtil.getValue("bankId");
		DWRUtil.removeAllOptions("bankId");
  		DWRUtil.addOptions("bankId",bankName);
  		DWRUtil.setValue("bankId",bankId);
  		
  		
	}
	
	function getrmtICWBank() 	
	{
		ITZCPMS.getICWRmtBankList(creatermtICWBank);
	}
	function creatermtICWBank(bankName) 
	{   
		var bankId = DWRUtil.getValue("bankId");
		DWRUtil.removeAllOptions("bankId");
  		DWRUtil.addOptions("bankId",bankName);
  		DWRUtil.setValue("bankId",bankId);
  		
  		
	}
	
	function getrmtBeneficiaryBank() 	
	{
		ITZCPMS.getRmtBeneficiaryBankList(createRmtBeneficiaryBank);
	}
	function createRmtBeneficiaryBank(bankName) 
	{   
		var bankId = DWRUtil.getValue("bankId");
		DWRUtil.removeAllOptions("bankId");
  		DWRUtil.addOptions("bankId",bankName);
  		DWRUtil.setValue("bankId",bankId);
  		
  		
	}
	
	function getStateName() 	
	{
		ITZCPMS.getState(createState);
	}
	function createState(stateName) 
	{
		var stateId =   DWRUtil.getValue("stateId");
		
		DWRUtil.removeAllOptions("stateId");
  		DWRUtil.addOptions("stateId",stateName);
  		DWRUtil.setValue("stateId",stateId);
  		
  		
	}
	
	
	function getCityName() 	
	{
		
		var stateCode = DWRUtil.getValue("stateId");
		ITZCPMS.getCity(createCity,stateCode);
	}
	function createCity(cityName) 
	{
		var cityId = DWRUtil.getValue("cityId");
		DWRUtil.removeAllOptions("cityId");
  		DWRUtil.addOptions("cityId",cityName);
  		DWRUtil.setValue("cityId",cityId);
  		
  		
	}
	
	function getRmtStateName() 	
	{
		ITZCPMS.getRmtState(createRmtState);
	}
	function createRmtState(stateName) 
	{
		var stateId =   DWRUtil.getValue("stateId");
		DWRUtil.removeAllOptions("stateId");
  		DWRUtil.addOptions("stateId",stateName);
  		DWRUtil.setValue("stateId",stateId);
  		
  		
	}
	
	function getRmtCityName() 	
	{
		
		var stateCode = DWRUtil.getValue("stateId");
		ITZCPMS.getRmtCity(createRmtCity,stateCode);
	}
	function createRmtCity(cityName) 
	{
		var cityId = DWRUtil.getValue("cityId");
		DWRUtil.removeAllOptions("cityId");
  		DWRUtil.addOptions("cityId",cityName);
  		DWRUtil.setValue("cityId",cityId);
  		
  		
	}
	
	function getRmtBranchName() 	
	{
		var bankCode = DWRUtil.getValue("bankId");
		var citycode = DWRUtil.getValue("cityId");
		var stateCode = DWRUtil.getValue("stateId");
		ITZCPMS.getRmtBranch(createRmtBranch,bankCode,citycode,stateCode);
	}
	function createRmtBranch(branchName) 
	{
		var rmtbranchId = DWRUtil.getValue("rmtbranchId");
		DWRUtil.removeAllOptions("rmtbranchId");
  		DWRUtil.addOptions("rmtbranchId",branchName);
  		DWRUtil.setValue("rmtbranchId",rmtbranchId);
  		
  		
	}
	
	function getIFSCCode(){
		var returnVal = null;
		var branchCode = DWRUtil.getValue("rmtbranchId");
		ITZCPMS.getIFSCCodeValue(branchCode,{async: false,callback:function(returnValDWR){returnVal = returnValDWR;}});
		DWRUtil.setValue("ifscCode",returnVal);
		}
	
	function getONUSFLAG(){
		var returnVal = null;
		var bankCode = DWRUtil.getValue("bankId");
		ITZCPMS.getONUSFLAGValue(bankCode,{async: false,callback:function(returnValDWR){returnVal = returnValDWR;}});
		DWRUtil.setValue("onusFlag",returnVal);
		}
	
	function getRmtFileUploadFormat(){
		var returnVal = null;
		var bankCode = DWRUtil.getValue("bankId");
		ITZCPMS.getRmtFileUploadFormatValue(bankCode,{async: false,callback:function(returnValDWR){returnVal = returnValDWR;}});
		DWRUtil.setValue("rmtFileExt",returnVal);
		}
	
	function getScufBranch() 	
	{
		ITZCPMS.getScufBranchList(createScufBranch);
	}
	
	function createScufBranch(branchName) 
	{   
		var branchId = DWRUtil.getValue("branchId");
		DWRUtil.removeAllOptions("branchId");
  		DWRUtil.addOptions("branchId",branchName);
  		DWRUtil.setValue("branchId",branchId);
    }
	
	function getEquitasBranch() 	
	{
		ITZCPMS.getEquitasBranchList(createEquitasBranch);
	}
	
	function createEquitasBranch(branchName) 
	{   
		var branchId = DWRUtil.getValue("execBranchid");
		DWRUtil.removeAllOptions("execBranchid");
  		DWRUtil.addOptions("execBranchid",branchName);
  		DWRUtil.setValue("execBranchid",branchId);
    }
	
	function  getMobileOperators(){
		ITZCPMS.getMobileOperators(createMobileOperators);
	}

	function createMobileOperators(operators) 
	{   
		var operatorId = DWRUtil.getValue("operator");
		DWRUtil.removeAllOptions("operator");
  		DWRUtil.addOptions("operator",operators);
  		DWRUtil.setValue("operator",operatorId);
    }
	
	function  getCircles(){
		var operatorId = DWRUtil.getValue("operator");
		ITZCPMS.getCircles(createCircles,operatorId);
	}

	function createCircles(circles) 
	{   
		var circleId = DWRUtil.getValue("circle");
		DWRUtil.removeAllOptions("circle");
  		DWRUtil.addOptions("circle",circles);
  		DWRUtil.setValue("circle",circleId);
    }
	function getJanalaxmiBranch() 	
	{
		ITZCPMS.getJanalaxmiBranchList(createJanalaxmiBranch);
	}
	
	function createJanalaxmiBranch(BRANCH_NAME) 
	{   
		var branchId = DWRUtil.getValue("execBranchid");
		DWRUtil.removeAllOptions("execBranchid");
  		DWRUtil.addOptions("execBranchid",BRANCH_NAME);
  		DWRUtil.setValue("execBranchid",branchId);
    }
	
	function getJanalaxmiCrecNames() 	
	{	
		var branchId = DWRUtil.getValue("execBranchid");
		ITZCPMS.getJanalaxmiCrecNamesList(createJanalaxmiCrecNames, branchId);
	}
	
	function createJanalaxmiCrecNames(CREC_NAME) 
	{   
		var CREC_EMP_ID = DWRUtil.getValue("CREC_EMP_ID");
		DWRUtil.removeAllOptions("CREC_EMP_ID");
  		DWRUtil.addOptions("CREC_EMP_ID",CREC_NAME);
  		DWRUtil.setValue("CREC_EMP_ID",CREC_EMP_ID);
    }