var date_format='DDMMYYYY'
$(function(){$('body').delegate(':input','focusin',function(){if($(this).hasClass('search')){return}
$(this).css('border-width','2px');$(this).css('border-color','#73879C');input_selected=true;if($(this).hasClass('passenger_detail')){passenger_detail=$(this)}
if($(this).hasClass('flight_detail')){flight_detail=$(this)}});$('body').delegate(':input','focusout',function(){if($(this).hasClass('search')){return}
$(this).css('border-width','1px');$(this).css('border-color','rgb(204, 204, 204)');input_selected=false;if($(this).hasClass('passenger_detail')){passenger_detail=null;}
if($(this).hasClass('flight_detail')){flight_detail=null;}});$('body').delegate(':input.currency','change',function(){a=$(this).val();if(isNaN(a)){alert("Please enter number");return}
$(this).val((Number(a)).toFixed(2));});$('body').delegate(':input.ddmmyyyy','change',function(){d=moment($(this).val(),date_format);if(!d.isValid()){alert('Error: Invalid Date');$(this).focus();return null;}
$(this).val(d.format('DD/MM/YYYY'));});has_attr=function(jq_object,attr){attr=jq_object.attr(attr);if(typeof attr!==typeof undefined&&attr!==false){return true;}
return false;}
roundUp=function(num,precision=0){precision=Math.pow(10,precision)
return Math.ceil(num*precision)/precision}
validate_date=function(jq_query=null,jq_object=null){if(jq_object==null)
date_field=$(jq_query);else{date_field=jq_object;}
d=moment(date_field.val(),date_format);if(!d.isValid()){date_field.focus();return 'Error: Invalid Date';}
return null;}
validate_select=function(jq_query=null,jq_object=null){if(jq_object==null)
sel=$(jq_query);else{sel=jq_object;}
if(sel.val()==-1){sel.focus();return 'Error: Please select one of the value';}
return null;}
validate_currency=function(jq_query=null,jq_object=null){if(jq_object==null)
amount=$(jq_query);else{amount=jq_object;}
if(isNaN(amount.val())){console.log(amount);amount.focus();return 'Error: Amount is not numeric.'}}
validate_currency_fields=function(){mes=null;$(':input.currency').each(function(){mes=validate_currency(jq_object=$(this));if(mes!=null){if(!has_attr($(this),'readonly')){return false}
else{mes=null}}});return mes}});