var date_format = 'DDMMYYYY'

$(function(){

	message = $('[name=message]');
	function get_valid_moment(jq_query){
		d = moment($(jq_query).val(), date_format);
		if($(jq_query).val() == ''){
			return d;
		}
		if(! d.isValid()){
			message.html('Error: Invalid Date');
			$(jq_query).focus();
			return null;
		}
		console.log(d);

		today = moment(moment.now());
		if(! today.isSameOrAfter(d, 'd')){
			message.html('Warning: Date is in future');
		}
		return d;
	}

	function has_attribute(jq_obj, attr){
		att = jq_obj.attr(attr);

		if(typeof att !== typeof undefined && att !== false){
			return true
		}
		return false
	}

	from_date = get_valid_moment('[name=rprt-from]');

	to_date = get_valid_moment('[name=rprt-to]');

	$('#get_report').on('click', function(){

		t_from_date = get_valid_moment('[name=rprt-from]');
		t_to_date = get_valid_moment('[name=rprt-to]');

		if(t_from_date == null || t_to_date == null){
			return;
		}

		else{
			if(t_from_date.isSameOrBefore(t_to_date)){
				console.log('requires new get');
				if(has_attribute($(this), 'disabled')){
					return;
				}
				$.blockUI();
				// $(this).html('Submit <span class="fa fa-refresh fa-spin"></span>')
				// $(this).attr('disabled', 'disabled')
				window.location.assign(window.location.pathname + '?from=' + moment(t_from_date).format('YYYY-MM-DD') + '&to=' + moment(t_to_date).format('YYYY-MM-DD'));
			}else{
				message.html('Error: Invalid range of date.');
			}
		}
	});

	$('#get_csv_report').on('click', function(){
		t_from_date = get_valid_moment('[name=rprt-from]');
		t_to_date = get_valid_moment('[name=rprt-to]');

		if(t_from_date == null || t_to_date == null){
			return;
		}

		if(t_from_date.isSameOrBefore(t_to_date)){
			console.log('requires new get');
			if(has_attribute($(this), 'disabled')){
				return;
			}
			// $.blockUI();
			// $(this).html('Submit <span class="fa fa-refresh fa-spin"></span>')
			// $(this).attr('disabled', 'disabled')
			window.location.assign(window.location.pathname + '/csv' + '?from=' + moment(t_from_date).format('YYYY-MM-DD') + '&to=' + moment(t_to_date).format('YYYY-MM-DD'));
		}else{
			message.html('Error: Invalid range of date.');
		}
	});

	calc_report_totals = function(){

		entry_rows = $('.entries tbody [name=entry]')

		console.log(entry_rows.length);
		elem = ['rprt-amount', 'rprt-cust-charge-net', 'rprt-sale-total', 'rprt-suppl-charge-net', 'rprt-commission-net', 'rprt-purchase-total'];
		totals = [0,0,0,0,0,0];
		entry_rows.each(function(){
			row = $(this);
			elem.forEach(function(value, index, arra){
				n = Number(row.find('[name='+value+']').html());
				totals[index] += n;
			});
		});

		elem.forEach(function(value, index){
			// console.log(totals);
			$('.entries tfoot tr').find('[name='+value+']').html(totals[index].toFixed(2));
		});
	}

	$('[name=rprt-page]').on('change', function(){
		t_from_date = get_valid_moment('[name=rprt-from]');
		t_to_date = get_valid_moment('[name=rprt-to]');

		if(t_from_date == null || t_to_date == null){
			return;
		}

		if(t_from_date.isSameOrBefore(t_to_date)){
			console.log(window.location.pathname + '?from=' + moment(t_from_date).format('YYYY-MM-DD') + '&to=' + moment(t_to_date).format('YYYY-MM-DD') + '&page=' + $('[name=rprt-page]').val());
			$.blockUI();
			window.location.assign(window.location.pathname + '?from=' + moment(t_from_date).format('YYYY-MM-DD') + '&to=' + moment(t_to_date).format('YYYY-MM-DD') + '&page=' + $('[name=rprt-page]').val() );
		}else{
			message.html('Error: Invalid range of date.');
		}

	});

	$('[name=rprt-from]').focus();
	calc_report_totals();
});
