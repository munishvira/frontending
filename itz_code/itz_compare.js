// itz_dict= {}
remittance_array = [];
// list2d = [];
$(function(){

  function readcsv(e) {
    var file = e.target.files[0];
    if (!file) {
      return;
    }
    var reader1 = new FileReader();
    reader1.onload = function(e) {
      var documents = e.target.result;
      remittance_array = $.csv.toArrays(documents);
    }
    reader1.readAsText(file);
    // delete file;
  }

  function readsecondFile(f) {
   var file2 = f.target.files[0];
   if (!file2) {
    return;
   }
   var reader2 = new FileReader();
   reader2.onload = function(f){
    var contents2 = f.target.result;
    displayContents(contents2);
    };
   reader2.readAsText(file2);
  }

  function displayContents(contents2) {
    var element2 = $('#itz-content');
    element2.append(contents2);
  }

  $('#remittance-input').on('change', readcsv);
  $('#itz-input').on('change', readsecondFile);

  // function get_itz_data(){
  //   var itz_row = $("#itz-content table tr.InfoListEvenRow,#itz-content table tr.InfoListOddRow")
  //   // console.log(remittance_array)
  //   // console.log(itz_row)
  //
  //   itz_row.each(function(){
  //     var array = [ ];
  //     c = $(this).find("TD")
  //     array = [c.eq(4).html(),c.eq(14).html(),c.eq(11).html()]
  //     list2d.push(array)
  //   });
  //   return list2d;
  // }

  function get_itz_dictionary2(){
    var itz_row = $("#itz-content table tr.InfoListEvenRow,#itz-content table tr.InfoListOddRow")
    var itz_dict = {};
    itz_row.each(function(){
      var array = [ ];
      c = $(this).find("TD")
      // array = [c.eq(4).html(),c.eq(14).html(),c.eq(11).html()]
      // list2d.push(array)
      var ph = c.eq(4).html()
      var date = c.eq(14).html()
      var amt = parseFloat(c.eq(11).html())
      if(itz_dict.hasOwnProperty(ph)){                           //finding the key phone number in the dictionary
        if(itz_dict[ph].hasOwnProperty(date)){                 //find the key date of second dictionary inside the key phone number
          (itz_dict[ph][date]).push(amt)                  //appending amount to the second dictionary for the following date
        }
        else {
          itz_dict[ph][date]=[amt]                        //checking for another date and creating list of amount for that date
        }
      }
      else{
        temp = {}
        temp[date] = [amt]
        itz_dict[ph] = temp                                  //checking for different phone numbers and if the not print the rest
      }
    });
    return itz_dict;
  }

  // function get_itz_dictionary(){
  //   for(i=0;i<list2d.length;i++){
  //     ph = list2d[i][0]
  //     date = list2d[i][1]
  //     amt = parseFloat(list2d[i][2])
  //     if(itz_dict.hasOwnProperty(ph)){                           //finding the key phone number in the dictionary
  //       if(itz_dict[ph].hasOwnProperty(date)){                 //find the key date of second dictionary inside the key phone number
  //         (itz_dict[ph][date]).push(amt)                  //appending amount to the second dictionary for the following date
  //       }
  //       else {
  //         itz_dict[ph][date]=[amt]                        //checking for another date and creating list of amount for that date
  //       }
  //     }
  //     else{
  //       temp = {}
  //       temp[date] = [amt]
  //       itz_dict[ph] = temp                                  //checking for different phone numbers and if the not print the rest
  //     }
  //   }
  //   return itz_dict ;
  // }
  get_split_amt = function(b){
    split_amt= []
    var amt = b
    var n = parseInt(amt/5000)
    var r = amt%5000
    while (amt > 0 && n > 0) {
      var a = Math.floor(amt / n / 5000) * 5000;
      amt -= a;
      n--;
      split_amt.push(a);
    }
    if(r != 0){
      split_amt.push(r)
    }
    return split_amt;
  }


  match_amt = function(itz,remittance){
    index_array = Array(remittance.length).fill(-1)
    var i=0,j=0;
    while(i<remittance.length && j<itz.length){
      if(itz[j] == remittance[i]){
         index_array[i] = j;
         i++,j++;
      }
      else{
        j++;
      }
    }
    for(i=0;i<remittance.length;i++){
      if(index_array[i] == -1){
        return null;
      }
    }
    return index_array;
  }

  $('button').on('click', function(){
   // itz_list = get_itz_data();
   // console.log(itz_list)
   itz_dict = get_itz_dictionary2();
   // console.log('dict');
   // console.log(itz_dict)
   // console.log(remittance_array.length);
   for(var j=1;j<remittance_array.length;j++){
    r_ph = remittance_array[j][2]
    r_ph = r_ph.replace(/ /g, '');
    remittance_array[j][2] = r_ph;
    r_date = remittance_array[j][3]
    r_amt = remittance_array[j][6]
    rt_amt = get_split_amt(r_amt)
    // continue
     if(remittance_array[j][4] == "ITZ MONEY TRF AC"){
      if(itz_dict.hasOwnProperty(r_ph)){
        if(itz_dict[r_ph].hasOwnProperty(r_date)){
          index_array = match_amt(itz_dict[r_ph][r_date],rt_amt)
          if(index_array == null){
            console.log("amt not found")
          }
          else{
            // delete_match_amt(itz_dict[r_ph][r_date],index_array)
            console.log("amt match found")
          }
        }
        else{
          console.log("not found")
        }
      }
     }
   }

   console.log('completed');
  });
});



// for (var i = 0; i<array2.length; i++) {
//     for (var j = 0; j<array1.length; j++) {
//         if (array2[i] == array1[j]) {
//             array1 = array1.slice(0, j).concat(array1.slice(j+1, array1.length));
//         }
//     }
// }
